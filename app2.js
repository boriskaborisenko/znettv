const requestify = require('requestify')
const execSh = require("exec-sh")
const vlc = '/Applications/VLC.app/Contents/MacOS/VLC'



const appRouter = (app, next) => {
    app.get('/ch/:id', (req,res)=>{
        execSh(['kill -9 $(pgrep VLC)', "echo VLC STARTED >&2"], true)
        const ch = req.params.id
        const url = 'http://91.210.251.170/channels/' + ch
        setTimeout(()=>{
            execSh([vlc + ' ' + url + ' ', "echo VLC STARTED >&2"], true)

            res.json({
                answer: 'answer',
                url: url
            })
        }, 10)
        
    })

    app.get('/quit',(req,res)=>{
       execSh(['kill -9 $(pgrep VLC)', "echo VLC STARTED >&2"], true)
       res.json({
           answer: 'answer',
           //url: url
       })
    })
}





module.exports = appRouter;